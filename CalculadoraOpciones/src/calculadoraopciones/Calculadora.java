/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraopciones;

/**
 *
 * @author alumno
 */
public class Calculadora extends javax.swing.JFrame {

    /**
     * Creates new form Calculadora
     */
    public Calculadora() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        operacion = new javax.swing.JComboBox<>();
        numero1 = new javax.swing.JTextField();
        numero2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        resultado = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 300));
        getContentPane().setLayout(null);

        operacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "+", "-", "x", "/" }));
        operacion.setMinimumSize(new java.awt.Dimension(40, 40));
        operacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                operacionActionPerformed(evt);
            }
        });
        getContentPane().add(operacion);
        operacion.setBounds(128, 107, 50, 30);

        numero1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        numero1.setText("0");
        numero1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                numero1KeyReleased(evt);
            }
        });
        getContentPane().add(numero1);
        numero1.setBounds(40, 107, 70, 30);

        numero2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        numero2.setText("0");
        numero2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                numero2KeyReleased(evt);
            }
        });
        getContentPane().add(numero2);
        numero2.setBounds(184, 107, 80, 30);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("=");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(271, 120, 8, 14);

        resultado.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        resultado.setText("0");
        getContentPane().add(resultado);
        resultado.setBounds(287, 107, 70, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void operacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_operacionActionPerformed

        int index=operacion.getSelectedIndex();
        String op=operacion.getItemAt(index);
        switch(op){
            case "+":
                double num1=Double.parseDouble(numero1.getText());
                double num2=Double.parseDouble(numero2.getText());
                double suma=num1+num2;
                resultado.setText(suma+"");
                break;
            case "-":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double resta=num1-num2;
                resultado.setText(resta+"");
                break;
            case "x":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double multiplicacion=num1*num2;
                resultado.setText(multiplicacion+"");
                break;
            case "/":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                if(numero2.getText().equals("0")){
                    resultado.setText("Error");
                }else{
                double division=num1/num2;
                
                resultado.setText(division+"");
                }
        }
    }//GEN-LAST:event_operacionActionPerformed

    private void numero1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numero1KeyReleased
        int index=operacion.getSelectedIndex();
        String op=operacion.getItemAt(index);
        switch(op){
            case "+":
                double num1=Double.parseDouble(numero1.getText());
                double num2=Double.parseDouble(numero2.getText());
                double suma=num1+num2;
                resultado.setText(suma+"");
                break;
            case "-":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double resta=num1-num2;
                resultado.setText(resta+"");
                break;
            case "x":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double multiplicacion=num1*num2;
                resultado.setText(multiplicacion+"");
                break;
            case "/":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                if(numero2.getText().equals("0")){
                    resultado.setText("Error");
                }else{
                double division=num1/num2;
                
                resultado.setText(division+"");
                }
        }
    }//GEN-LAST:event_numero1KeyReleased

    private void numero2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numero2KeyReleased
        int index=operacion.getSelectedIndex();
        String op=operacion.getItemAt(index);
        switch(op){
            case "+":
                double num1=Double.parseDouble(numero1.getText());
                double num2=Double.parseDouble(numero2.getText());
                double suma=num1+num2;
                resultado.setText(suma+"");
                break;
            case "-":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double resta=num1-num2;
                resultado.setText(resta+"");
                break;
            case "x":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                double multiplicacion=num1*num2;
                resultado.setText(multiplicacion+"");
                break;
            case "/":
                num1=Double.parseDouble(numero1.getText());
                num2=Double.parseDouble(numero2.getText());
                if(numero2.getText().equals("0")){
                    resultado.setText("Error");
                }else{
                double division=num1/num2;
                
                resultado.setText(division+"");
                }
                break;
        }
    }//GEN-LAST:event_numero2KeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calculadora().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField numero1;
    private javax.swing.JTextField numero2;
    public javax.swing.JComboBox<String> operacion;
    private javax.swing.JTextField resultado;
    // End of variables declaration//GEN-END:variables
}
